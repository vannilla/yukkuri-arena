// Yukkuri Arena - a web technologies-based game about pastries.
// Copyright (C) 2021 Alessio Vanni

// This file is part of Yukkuri Arena.

// Yukkuri Arena is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.

// Yukkuri Arena is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with Yukkuri Arena.  If not, see <https://www.gnu.org/licenses/>.

"use strict";

YukkuriArena.DataBase = (function () {
    let index = 0;

    function make(/* ... */) {
	let o = {};

	o['__hey_dont_mess_with_this_please'] = index;
	++index;

	Object.defineProperty(o, 'id', {
	    get: function () {
		return this.__hey_dont_mess_with_this_please;
	    },
	});

	if (0 !== arguments.length % 2) {
	    throw new Error("missing arguments in `make' function");
	}

	for (let i=0; i<arguments.length; i=i+2) {
	    let key = arguments[i];
	    let value = arguments[i+1];

	    Object.defineProperty(o, key, {
		get: function () {
		    if (0 === this.id) {
			throw new Error('invalid index');
		    }
		    return value;
		},
	    });
	}

	return o;
    }

    return [
	make('name', 'invalid',
	     'basehp', 0,
	     'baseat', 0,
	     'basedef', 0,
	     'basespd', 0,
	     'skills', []),
	make('name', 'Reimu',
	     'basehp', 100,
	     'baseatk', 10,
	     'basedef', 7,
	     'basespd', 6,
	     'skills', [1, 4]),
	make('name', 'Marisa',
	     'basehp', 95,
	     'baseatk', 10,
	     'basedef', 5,
	     'basespd', 7,
	     'skills', [2]),
	make('name', 'Alice',
	     'basehp', 100,
	     'baseatk', 11,
	     'basedef', 6,
	     'basespd', 6,
	     'skills', [3]),
	make('name', 'Myon',
	     'basehp', 90,
	     'baseatk', 12,
	     'basedef', 5,
	     'basespd', 7,
	     'skills', [5]),
	make('name', 'Remilia',
	     'basehp', 95,
	     'baseatk', 10,
	     'basedef', 5,
	     'basespd', 8,
	     'skills', [6]),
	make('name', 'Tewi',
	     'basehp', 100,
	     'baseatk', 8,
	     'basedef', 6,
	     'basespd', 5,
	     'skills', [7]),
	make('name', 'Kaguya',
	     'basehp', 100,
	     'baseatk', 5,
	     'basedef', 8,
	     'basespd', 6,
	     'skills', [8]),
	make('name', 'Mokou',
	     'basehp', 100,
	     'baseatk', 10,
	     'basedef', 5,
	     'basespd', 6,
	     'skills', [9]),
	make('name', 'Akyuun',
	     'basehp', 90,
	     'baseatk', 11,
	     'basedef', 5,
	     'basespd', 5,
	     'skills', [10]),
	make('name', 'Mystia',
	     'basehp', 100,
	     'baseatk', 7,
	     'basedef', 7,
	     'basespd', 6,
	     'skills', [1, 11]),
	make('name', 'Medicine',
	     'basehp', 90,
	     'baseatk', 6,
	     'basedef', 6,
	     'basespd', 5,
	     'skills', [12]),
	make('name', 'Eternity',
	     'basehp', 100,
	     'baseatk', 7,
	     'basedef', 4,
	     'basespd', 7,
	     'skills', [13]),
	make('name', 'Sannyo',
	     'basehp', 115,
	     'baseatk', 5,
	     'basedef', 7,
	     'basespd', 5,
	     'skills', [14]),
    ];
})();
