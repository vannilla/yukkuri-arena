// Yukkuri Arena - a web technologies-based game about pastries.
// Copyright (C) 2021 Alessio Vanni

// This file is part of Yukkuri Arena.

// Yukkuri Arena is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.

// Yukkuri Arena is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with Yukkuri Arena.  If not, see <https://www.gnu.org/licenses/>.

"use strict";

YukkuriArena.Teams = {};

YukkuriArena.Teams.teamCollection = [];

YukkuriArena.Teams.toastTimers = [];

YukkuriArena.Teams.renderTeambox = function (team) {
    function makeLi(contents) {
	let li = YukkuriArena.Common.element('li');
	li.textContent = contents;
	return li;
    }

    let cont = YukkuriArena.Common.element('div');
    let h4 = YukkuriArena.Common.element('h4');
    h4.textContent = team[1];
    h4.classList.toggle('team-name');
    cont.append(h4);

    let div = YukkuriArena.Common.element('div');
    div.classList.toggle('teambox');

    let ul = YukkuriArena.Common.element('ul');
    ul.append(makeLi(YukkuriArena.DataBase[team[2]].name));
    ul.append(makeLi(YukkuriArena.DataBase[team[3]].name));
    ul.append(makeLi(YukkuriArena.DataBase[team[4]].name));
    div.append(ul);

    let button = YukkuriArena.Common.element('button');
    button.innerHTML = 'Edit Team';
    button.onclick = function (e) {
	YukkuriArena.Teams.renderTeamEditor('team-editor', team);
	YukkuriArena.Teams.openCloseEditor('team-editor');
    };
    div.append(button);

    cont.append(div);

    return cont;
};

YukkuriArena.Teams.updateTeambox = function (team) {
    let box = document.querySelectorAll('.teambox')[team[0]];
    let h4 = document.querySelectorAll('.team-name')[team[0]];
    let lis = box.querySelectorAll('li');

    h4.innerHTML = team[1];
    lis[0].textContent = YukkuriArena.DataBase[team[2]].name;
    lis[1].textContent = YukkuriArena.DataBase[team[3]].name;
    lis[2].textContent = YukkuriArena.DataBase[team[4]].name;

    return box;
};

YukkuriArena.Teams.renderTeamEditor = function (id, team) {
    let header = document.getElementById(id + '-header');
    header.innerHTML = 'Editing ' + team[1];

    let inputs = document.querySelectorAll('.'+id+'-ids input');
    inputs[0].value = team[2];
    inputs[1].value = team[3];
    inputs[2].value = team[4];

    let button = document.getElementById(id + '-save');
    button.onclick = function () {
	let inputs = document.querySelectorAll('.'+id+'-ids input');
	let a = parseInt(inputs[0].value);
	let b = parseInt(inputs[1].value);
	let c = parseInt(inputs[2].value);

	if (!YukkuriArena.DataBase[a] ||
	    !YukkuriArena.DataBase[b] ||
	    !YukkuriArena.DataBase[c]) {
	    throw 'one of the selected units does not exist';
	}

	team[2] = a;
	team[3] = b;
	team[4] = c;

	team[1] =
	    document.querySelector('input[name="team-name"]').value || team[1];

	YukkuriArena.Teams.editTeam(team);
	YukkuriArena.Teams.updateTeambox(team);
	YukkuriArena.Teams.openCloseEditor(id);
    };
};

YukkuriArena.Teams.openCloseEditor = function (id) {
    let modal = document.getElementById(id);
    modal.classList.toggle('modal-hidden');
    return modal;
};

YukkuriArena.Teams.editTeam = function (team) {
    let allteams = YukkuriArena.Teams.teamCollection;

    if (allteams[0][0] === team[0]) {
	allteams[0][1] = team[1];
	allteams[0][2] = team[2];
	allteams[0][3] = team[3];
	allteams[0][4] = team[4];
	return team;
    } else if (allteams[1][0] === team[0]) {
	allteams[1][1] = team[1];
	allteams[1][2] = team[2];
	allteams[1][3] = team[3];
	allteams[1][4] = team[4];
	return team;
    } else if (allteams[2][0] === team[0]) {
	allteams[2][1] = team[1];
	allteams[2][2] = team[2];
	allteams[2][3] = team[3];
	allteams[2][4] = team[4];
	return team;
    }

    throw "why can't the team be found?";
};

YukkuriArena.Teams.saveTeams = function () {
    let stored = btoa(JSON.stringify(YukkuriArena.Teams.teamCollection));
    localStorage.setItem('teams', stored);
    YukkuriArena.Teams.notify();
    return stored;
};

YukkuriArena.Teams.loadTeams = function () {
    let stored = localStorage.getItem('teams');
    if (null === stored) {
	return [
	    [0, "Team 1", 1,1,1],
	    [1, "Team 2", 1,1,1],
	    [2, "Team 3", 1,1,1],
	];
    }
    return JSON.parse(atob(stored));
};

YukkuriArena.Teams.notify = function () {
    let box = document.getElementById('notifications');
    let toast = YukkuriArena.Common.element('div');
    toast.classList.add('toast');
    toast.innerText = 'All teams saved.';
    toast.dataset.timerid = setTimeout(function (e) {
	e.classList.toggle('toast-hidden');
	e.onanimationend = function (e) {
	    e.target.remove();
	};
    }, 3000, toast);
    toast.onclick = function (e) {
	clearTimeout(e.target.dataset.timerid);
	e.target.classList.toggle('toast-hidden');
	e.target.onanimationend = function (e) {
	    e.target.remove();
	};
    };
    box.insertBefore(toast, box.firstChild);
};
