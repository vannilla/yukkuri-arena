// Yukkuri Arena - a web technologies-based game about pastries.
// Copyright (C) 2021 Alessio Vanni

// This file is part of Yukkuri Arena.

// Yukkuri Arena is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.

// Yukkuri Arena is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with Yukkuri Arena.  If not, see <https://www.gnu.org/licenses/>.

"use strict";

YukkuriArena.Skills = {};

YukkuriArena.Skills.DataBase = (function () {
    let index = 0;

    function make(/* ... */) {
	let o = {};

	o['__hey_dont_mess_with_this_please'] = index;
	++index;

	Object.defineProperty(o, 'id', {
	    get: function () {
		return this.__hey_dont_mess_with_this_please;
	    },
	});

	if (0 !== arguments.length % 2) {
	    throw new Error("missing arguments in `make' function");
	}

	for (let i=0; i<arguments.length; i=i+2) {
	    let key = arguments[i];
	    let value = arguments[i+1];

	    Object.defineProperty(o, key, {
		get: function () {
		    if (0 === this.id) {
			throw new Error('invalid index');
		    }
		    return value;
		},
	    });
	}

	return o;
    }

    function composed(/* ... */) {
	let o = {};

	if (0 !== arguments.length % 2) {
	    throw new Error("missing arguments in `make' function");
	}

	for (let i=0; i<arguments.length; i=i+2) {
	    let key = arguments[i];
	    let value = arguments[i+1];

	    Object.defineProperty(o, key, {
		get: function () {
		    if (0 === this.id) {
			throw new Error('invalid index');
		    }
		    return value;
		},
	    });
	}

	return o;
    }

    function statname(property) {
	switch (property) {
	case 'atk':
	    return 'attack';
	case 'def':
	    return 'defense';
	case 'spd':
	    return 'speed';
	}

	return 'undefined';
    }

    function reducer(property, value, time) {
	return function (a, b) {
	    YukkuriArena.Skills.multiplier(b, property, value, time);
	    let name = YukkuriArena.Common.boldface(statname(property));
	    let pc = YukkuriArena.Common.boldface(100 - (value * 100));
	    let u = YukkuriArena.Common.teamColor(b);
	    return `${u} had its ${name} reduced by ${pc}%!`;
	};
    }

    function increaser(property, value, time) {
	return function (a, b) {
	    YukkuriArena.Skills.multiplier(b, property, value, time);
	    let name = YukkuriArena.Common.boldface(statname(property));
	    let pc = YukkuriArena.Common.boldface((value * 100) - 100);
	    let u = YukkuriArena.Common.teamColor(b);
	    return `${u} had its ${name} increased by ${pc}%!`;
	};
    }

    function damager(value) {
	return function (a, b) {
	    let pc = YukkuriArena.Common.boldface(value * 100);
	    let damage = YukkuriArena.Skills.attack(a, b, value);
	    let u = YukkuriArena.Common.teamColor(a);
	    let t = YukkuriArena.Common.teamColor(b);
	    damage = YukkuriArena.Common.boldface(damage);
	    return `${u} attacked ${t} with ${pc}% its attack for ${damage} damage!`;
	};
    }

    function fixeder(value) {
	return function (a, b) {
	    YukkuriArena.Skills.fixed(b, value);
	    let pc = YukkuriArena.Common.boldface(value);
	    let u = YukkuriArena.Common.teamColor(b);
	    return `${u} received a fixed damage of ${pc}!`;
	};
    }

    function healer(multiplier) {
	return function (a, b) {
	    let total = YukkuriArena.Skills.heal(a, b, multiplier);
	    total = YukkuriArena.Common.boldface(total);
	    let u = YukkuriArena.Common.teamColor(b);
	    return `${u} was healed by ${total}!`;
	};
    };

    function absorber(damage, heal) {
	return function (a, b) {
	    let effect = YukkuriArena.Skills.absorb(a, b, damage, heal);
	    let q = YukkuriArena.Common.boldface(effect.damage);
	    let p = YukkuriArena.Common.boldface(effect.healed);
	    let u = YukkuriArena.Common.teamColor(a);
	    let t = YukkuriArena.Common.teamColor(b);
	    return `${u} attacked ${t} for ${q} damage and healed back ${p} damage!`;
	};
    }

    function randomer(min, max, chance) {
	return function (a, b) {
	    let random = YukkuriArena.Common.random(0, 100);
	    let damage = (50 <= random) ? max : min;
	    return ((damager(damage))(a, b));
	};
    }

    function randomfixeder(min, max, chance) {
	return function (a, b) {
	    let random = YukkuriArena.Common.random(0, 100);
	    let damage = (50 <= random) ? max : min;
	    return ((fixeder(damage))(a, b));
	};
    }

    function statuser(status, time) {
	return function (a, b) {
	    YukkuriArena.Skills.status(b, status, time);
	    let name = YukkuriArena.Common.teamColor(b);
	    let p = YukkuriArena.Common.boldface(status);
	    return `${name} was inflicted with ${p}!`;
	};
    }

    function poisoner(chance, time) {
	return function (a, b) {
	    let random = YukkuriArena.Common.random(0, 100);
	    if (random > chance) {
		let name = YukkuriArena.Common.teamColor(b);
		return `${name} was not poisoned`;
	    }
	    return ((statuser('poison', time))(a, b));
	};
    }

    function composer(...args) {
	return args.filter(function (v) {
	    return 'object' === typeof v &&
		v.area &&
		'function' === typeof v.effect;
	});
    }

    return [
	make('name', 'invalid',
	     'cooldown', 0,
	     'description', 'Not a skill!',
	     'area', 0,
	     'team', 'player',
	     'effect', function (a, b) { return 'Nothing happened!'; }),
	make('name', 'Easy Song',
	     'cooldown', 18,
	     'description', 'Reduce all enemies\' speed by 10% for 7 turns',
	     'area', -1,
	     'team', 'enemy',
	     'effect', reducer('spd', 0.9, 7)),
	make('name', 'Excalibur',
	     'cooldown', 29,
	     'description', 'Always deal 30 damage to one enemy unit',
	     'area', 1,
	     'team', 'enemy',
	     'effect', fixeder(30)),
	make('name', 'City-sect Affection',
	     'cooldown', 20,
	     'description', 'Reduce one enemy\'s attack by 20% for 7 turns',
	     'area', 1,
	     'team', 'enemy',
	     'effect', reducer('atk', 0.8, 7)),
	make('name', 'Astron',
	     'cooldown', 22,
	     'description', 'Increase own\'s defense by 20% for 7 turns',
	     'area', 1,
	     'team', 'self',
	     'effect', increaser('def', 1.2, 7)),
	make('name', 'Roukanken',
	     'cooldown', 25,
	     'description', 'Cut next to everything (all enemy units) for 95% attack',
	     'area', -1,
	     'team', 'enemy',
	     'effect', damager(0.95)),
	make('name', 'Vampire~☆',
	     'cooldown', 26,
	     'description', 'Bite one enemy unit for 90% attack and heal for 75% the damage dealt',
	     'area', 1,
	     'team', 'enemy',
	     'effect', absorber(0.9, 0.75)),
	make('name', 'Wabbit',
	     'cooldown', 16,
	     'description', 'Will luck be on your side? 50% chance of 50 damage; 50% chance of 0 damage! (On one unit)',
	     'area', 1,
	     'team', 'enemy',
	     'effect', randomfixeder(0, 50, 50)),
	make('name', 'Immortal Paste',
	     'cooldown', 29,
	     'description', 'Heal team by 150% the healing; bind self 11 turns',
	     'area', -1,
	     'team', 'player',
	     'effect', composer(composed('area', -1,
					 'team', 'player',
					 'effect', healer(1.5)),
				composed('area', 1,
					 'team', 'self',
					 'effect', statuser('bind', 11)))),
	make('name', 'Immortal Flame',
	     'cooldown', 29,
	     'description', 'Burn all enemy units for 200% attack; bind self 11 turns',
	     'area', -1,
	     'team', 'enemy',
	     'effect', composer(composed('area', -1,
					 'team', 'enemy',
					 'effect', damager(2)),
				composed('area', 1,
					 'team', 'self',
					 'effect', statuser('bind', 11)))),
	make('name', 'AQN Hammer',
	     'cooldown', 29,
	     'description', 'Always deal 28 damage to two enemy units',
	     'area', 2,
	     'team', 'enemy',
	     'effect', fixeder(28)),
	make('name', 'Myschirp',
	     'cooldown', 20,
	     'description', 'Raise team\'s attack by 20% for 7 turns',
	     'area', -1,
	     'team', 'player',
	     'effect', increaser('atk', 1.2, 7)),
	make('name', 'Poison Touch',
	     'cooldown', 27,
	     'description', 'Damage for 35% attack and inflict poison to one enemy unit with 100% chance for 7 turns',
	     'area', 1,
	     'team', 'enemy',
	     'effect', composer(composed('area', 1,
					 'team', 'enemy',
					 'effect', damager(0.35)),
				composed('area', 1,
					 'team', 'enemy',
					 'effect', poisoner(100, 7)))),
	make('name', 'Spore Mist',
	     'cooldown', 27,
	     'description', '35% chance to poison an enemy for 12 turns, for all enemies',
	     'area', -1,
	     'team', 'enemy',
	     'effect', poisoner(35, 12)),
	make('name', 'Smoke Gust',
	     'cooldown', 26,
	     'description', '65% chance to poison an enemy for 7 turns, for all enemies',
	     'area', -1,
	     'team', 'enemy',
	     'effect', poisoner(65, 7)),
    ];
})();

YukkuriArena.Skills.multiplier = function (target, property, value, time) {
    target[property + 'mult'].push({value: value, time: time || 5});
    return value;
};

YukkuriArena.Skills.fixed = function (target, damage) {
    target['hp'] = Math.max(0, target['hp'] - damage);
    return damage;
};

YukkuriArena.Skills.stat = function (unit, property) {
    let mult = unit[property + 'mult'].reduce(function (a, v) {
	return a * v.value;
    }, 1);

    return unit[property] * mult;
};

YukkuriArena.Skills.attack = function (attacker, defender, multiplier) {
    let fullatk = Math.max(1, YukkuriArena.Skills.stat(attacker, 'atk'));
    let fulldef = Math.max(1, YukkuriArena.Skills.stat(defender, 'def'));

    let skillmult = (undefined === multiplier) ? 1 : multiplier;
    let damage = skillmult * 0.7 * fullatk * (fullatk/fulldef);
    let variance = (20 * damage)/100;
    let random = Math.round(YukkuriArena.Common.random(-variance, variance));
    let total = Math.floor(damage + random);

    defender['hp'] = Math.max(0, defender['hp'] - total);

    return total;
};

YukkuriArena.Skills.heal = function (healer, healed, multiplier) {
    let fullatk = Math.max(1, YukkuriArena.Skills.stat(healer, 'atk'));
    let fulldef = Math.max(1, YukkuriArena.Skills.stat(healer, 'def'));
    let fullspd = Math.max(1, YukkuriArena.Skills.stat(healer, 'spd'));

    let skillmult = (undefined === multiplier) ? 1.2 : multiplier;
    let heal = skillmult * ((0.8 * fullatk) + (1.1 * fulldef) + (0.7 * fullspd));
    let total = Math.floor(heal);

    healed['hp'] = Math.min(healed['maxhp'], healed['hp'] + total);

    return total;
};

YukkuriArena.Skills.absorb = function (attacker, defender, multiplier, healing) {
    let damage = YukkuriArena.Skills.attack(attacker, defender, multiplier);
    let healmult = (undefined === healing) ? 0.5 : healing;
    let healed = Math.floor(healmult * damage);

    attacker['hp'] = Math.min(attacker['maxhp'], attacker['hp'] + healed);

    return {damage: damage, healed: healed};
};

YukkuriArena.Skills.status = function (target, status, time) {
    let filtered = target.states.findIndex(function (v) {return v.name===status});
    if (-1 === filtered) {
	target.states.push({name: status, power: 1, time: time || 5});
    } else {
	target.states[filtered].time += time;
	target.states[filtered].power += 1;
    }
    return status;
};
