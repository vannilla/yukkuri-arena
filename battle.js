// Yukkuri Arena - a web technologies-based game about pastries.
// Copyright (C) 2021 Alessio Vanni

// This file is part of Yukkuri Arena.

// Yukkuri Arena is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.

// Yukkuri Arena is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with Yukkuri Arena.  If not, see <https://www.gnu.org/licenses/>.

"use strict";

YukkuriArena.Battle = {};

YukkuriArena.Battle.playerTeam = [];

YukkuriArena.Battle.enemyTeam = [];

YukkuriArena.Battle.turnQueue = [];

YukkuriArena.Battle.paused = false;

YukkuriArena.Battle.enemyTeamDataBase = [
    [0, "Team Reimu", 1,1,1],
    [1, "Marisa Exploration Party", 2,2,2],
    [2, "Alice Troupe", 3,3,3],
    [3, "Do You Like Marisa?", 3, 2, 3],
    [4, "Hakurei Temple", 1, 2, 5],
    [5, "Imperishable Night", 6, 8, 7],
];

YukkuriArena.Battle.renderTeambox = function (team) {
    let div = YukkuriArena.Common.element('div');
    div.classList.toggle('teambox');

    let h4 = YukkuriArena.Common.element('h4');
    h4.textContent = team[1];
    h4.classList.toggle('team-name');
    div.append(h4);

    let button = YukkuriArena.Common.element('button');
    button.innerHTML = 'Use Team';
    button.onclick = function (e) {
	YukkuriArena.Battle.playerTeam = YukkuriArena.Battle.cloneTeam(team);
	document.getElementById('summary').hidden = false;
	document.getElementById('team-picker').hidden = true;
	YukkuriArena.Battle.renderSummary(YukkuriArena.Battle.playerTeam);
	for (let i=1; i<4; ++i) {
	    YukkuriArena.Battle.playerTeam[i].team = 'player';
	}
    };
    div.append(button);

    return div;
};

YukkuriArena.Battle.renderSummary = function (team) {
    document.getElementById('summary-team-name').textContent = team[0];
    document.getElementById('summary-begin').onclick = function () {
	document.getElementById('summary').hidden = true;
	document.getElementById('battle-log').hidden = false;
	document.getElementById('teams-stats').hidden = false;
	document.getElementById('battle-turns').hidden = false;
	YukkuriArena.Battle.enemyTeam = YukkuriArena.Battle.pickEnemyTeam();
	for (let i=1; i<4; ++i) {
	    YukkuriArena.Battle.enemyTeam[i].team = 'enemy';
	}
	YukkuriArena.Battle.report(
	    'You are fighting against '
		+ YukkuriArena.Common.boldface(YukkuriArena.Battle.enemyTeam[0])
		+ '!'
	);
	YukkuriArena.Battle.initTeamStats(YukkuriArena.Battle.playerTeam,
					  YukkuriArena.Battle.enemyTeam);
	YukkuriArena.Battle.turnQueue =
	    YukkuriArena.Battle.initQueue(YukkuriArena.Battle.playerTeam,
					  YukkuriArena.Battle.enemyTeam);
	YukkuriArena.Battle.renderUnitOrder();

	window.setTimeout(YukkuriArena.Battle.tick, 1355);
    };
};

YukkuriArena.Battle.initTeamStats = function (player, enemy) {
    let template = document.getElementById('unit-stats-template').innerHTML;
    let teamcontainer = document.getElementById('teams-stats');
    let ending = document.getElementById('pause-resume-button');

    for (let i=1; i<player.length; ++i) {
	let patterns = [
	    {pattern: '%id', value: 'pu'+player[i].position},
	    {pattern: '%class', value: 'unit-player'},
	    {pattern: '#', value: 'unit-stats'},
	];
	let d = YukkuriArena.Common.fromTemplate(template, ...patterns);
	teamcontainer.insertBefore(d, ending);
	d.querySelector('.unit-stats-name').textContent = player[i].name;
	d.querySelector('.unit-stats-hp').textContent = player[i].hp;
	d.querySelector('.unit-stats-maxhp').textContent = player[i].maxhp;
	d.querySelector('.unit-stats-mults').textContent = `⚔: 1.00 🛡: 1.00 🏃: 1.00`;
	d.querySelector('.unit-stats-states').textContent = `⛓: 0 💧: 0/0`;
    }

    for (let i=1; i<enemy.length; ++i) {
	let patterns = [
	    {pattern: '%id', value: 'eu'+player[i].position},
	    {pattern: '%class', value: 'unit-enemy'},
	    {pattern: '#', value: 'unit-stats'},
	];
	let d = YukkuriArena.Common.fromTemplate(template, ...patterns);
	teamcontainer.insertBefore(d, ending);
	d.querySelector('.unit-stats-name').textContent = enemy[i].name;
	d.querySelector('.unit-stats-hp').textContent = enemy[i].hp;
	d.querySelector('.unit-stats-maxhp').textContent = enemy[i].maxhp;
	d.querySelector('.unit-stats-mults').textContent = `⚔: 1.00 🛡: 1.00 🏃: 1.00`;
	d.querySelector('.unit-stats-states').textContent = `⛓: 0 💧: 0/0`;
    }

    return null;
};

YukkuriArena.Battle.renderUnitStats = function (unit) {
    let prefix = (unit.team === 'player') ? 'pu' : 'eu';
    let position = unit.position;
    let container = document.getElementById(prefix + position);
    let hp = container.querySelector('.unit-stats-hp');
    let mhp = container.querySelector('.unit-stats-maxhp');
    let mults = container.querySelector('.unit-stats-mults');
    let states = container.querySelector('.unit-stats-states');

    let reductor = function (a, v) {
	return a * v.value;
    };

    let namefilterer = function (name) {
	return function (v) {
	    return v.name === name;
	};
    };
    let timereductor = function (a, v) {
	return a + v.time;
    };

    let atkmult = unit.atkmult.reduce(reductor, 1).toFixed(2);
    let defmult = unit.defmult.reduce(reductor, 1).toFixed(2);
    let spdmult = unit.spdmult.reduce(reductor, 1).toFixed(2);

    let bind = unit.states.find(namefilterer('bind'));
    let poison = unit.states.find(namefilterer('poison'));

    let bindtime = (bind) ? bind.time : 0;
    let poisontime = (poison) ? poison.time : 0;
    let poisontype = (poison) ? poison.power : 0;

    hp.textContent = unit.hp;
    mhp.textContent = unit.maxhp;
    mults.textContent = `⚔: ${atkmult} 🛡: ${defmult} 🏃: ${spdmult}`;
    states.textContent = `⛓: ${bindtime} 💧: ${poisontype}/${poisontime}`;

    return unit;
};

YukkuriArena.Battle.renderUnitOrder = function () {
    let container = document.getElementById('battle-turns');
    container.innerHTML = '';

    let template = document.getElementById('unit-turn-template').innerHTML;

    for (let i=0; i<5; ++i) {
	let unit = YukkuriArena.Battle.turnQueue[i];
	let patterns = [
	    {pattern: '%class', value: `unit-turn-${unit.team}`},
	    {pattern: '#', value: 'unit-turn'},
	];
	let d = YukkuriArena.Common.fromTemplate(template, ...patterns);
	container.appendChild(d);
	d.querySelector('.unit-turn-name').innerHTML =
	    `${unit.name} [${unit.position}]`;
    }

    return null;
};

YukkuriArena.Battle.pickEnemyTeam = function () {
    let total = YukkuriArena.Battle.enemyTeamDataBase.length;
    let rand = YukkuriArena.Common.random(0, total-1);
    return YukkuriArena.Battle.cloneTeam(
	YukkuriArena.Battle.enemyTeamDataBase[rand]
    );
};

YukkuriArena.Battle.clone = function (unit) {
    let o = {};

    o.id = unit.id;
    o.name = unit.name;
    o.maxhp = unit.basehp;
    o.hp = unit.basehp;
    o.atk = unit.baseatk;
    o.def = unit.basedef;
    o.spd = unit.basespd;

    o.times = 0;

    o.atkmult = [];
    o.defmult = [];
    o.spdmult = [];

    o.states = [];

    o.skills = [];
    o.basecd = [];
    o.cooldowns = [];
    for (let i in unit.skills) {
	let skill = YukkuriArena.Skills.DataBase[unit.skills[i]];
	o.skills[i] = skill.id
	o.basecd[i] = o.cooldowns[i] = skill.cooldown;
    }

    return o;
};

YukkuriArena.Battle.cloneTeam = function (team) {
    let selected = [];
    selected[0] = team[1];
    for (let i=2; i<5; ++i) {
	selected[i-1] =
	    YukkuriArena.Battle.clone(YukkuriArena.DataBase[team[i]]);
	selected[i-1].position = i-1;
    }
    return selected;
};

YukkuriArena.Battle.sortQueue = function (queue) {
    return queue.sort(function (a, b) {
	let fullspdb = Math.max(1, YukkuriArena.Skills.stat(b, 'spd'));
	let fullspda = Math.max(1, YukkuriArena.Skills.stat(a, 'spd'));
	let res = fullspdb - fullspda;
	let times = b.times - a.times;
	return (times > 2) ?
	    -1 :
	    (times > 1 || 0 === res) ?
	    YukkuriArena.Common.random(0, 3) - 1 :
	    res;
    });
};

YukkuriArena.Battle.initQueue = function (player, enemy) {
    let team1 = player.slice(1);
    let team2 = enemy.slice(1);
    return YukkuriArena.Battle.sortQueue(team1.concat(team2));
};

YukkuriArena.Battle.nextUnit = function () {
    return YukkuriArena.Battle.turnQueue.shift();
};

YukkuriArena.Battle.restoreUnit = function (unit) {
    YukkuriArena.Battle.turnQueue.push(unit);
    return YukkuriArena.Battle.sortQueue(YukkuriArena.Battle.turnQueue);
};

YukkuriArena.Battle.nextTick = function () {
    // return Math.floor((Math.random() * 1.7 + 0.5) * 1000);
    return 1355;
};

YukkuriArena.Battle.selectTeam = function (team) {
    return YukkuriArena.Battle[team+'Team'].slice(1);
};

YukkuriArena.Battle.selectTarget = function (unit, team) {
    if (undefined === team) {
	throw 'missing team';
    }

    if ('self' === team) {
	// TODO: will units ever self-destruct? I hope not.
	return {index: -1, target: unit};
    }

    if (1 === team.length-1) {
	return {index: 1, target: team[0]};
    }

    // TODO: allow for different types of targeting
    let rand = YukkuriArena.Common.random(0, team.length-1);
    let target = {index: rand+1, target: team[rand]};

    return target;
};

YukkuriArena.Battle.pickTarget = function (unit, index, team) {
    if (undefined === team) {
	throw 'missing team';
    }

    if ('self' === team) {
	// TODO: will units ever self-destruct? I hope not.
	return {index: index, target: unit};
    }

    if (1 === team.length-1) {
	return {index: 1, target: team[0]};
    }

    let target = {index: index, target: team[index]};

    return target;
};

YukkuriArena.Battle.perform = function (unit, effect) {
    let action = effect.effect;
    let target = effect.target;

    let hlactor = YukkuriArena.Common.teamColor(unit);
    let hltarget = YukkuriArena.Common.teamColor(target.target);
    let result = [];

    if (0 < unit.states.filter(function (v) {return(v.name==='bind')}).length) {
	result.push({
	    report: `${hlactor} could not act!`,
	    actions: [],
	});
    } else  if ('skill' === effect.type) {
	result.push({
	    report: action(unit, target.target),
	    actions: [],
	});
    } else {
	let damage = action(unit, target.target);
	damage = YukkuriArena.Common.boldface(damage);
	result.push({
	    report: `${hlactor} attacked ${hltarget} for ${damage} damage.`,
	    actions: [],
	});
    }

    if (target.target.hp <= 0) {
	result.push({
	    report: `${hltarget} has been defeated!`,
	    actions: [
		(function (a, b) {
		    return function () {
			((a.team === 'player') ?
			 YukkuriArena.Battle.enemyTeam :
			 YukkuriArena.Battle.playerTeam).splice(b, 1);
		    };
		})(unit, target.index),
	    ],
	});
    }

    return result;
};

YukkuriArena.Battle.tick = function () {
    YukkuriArena.Battle.turnQueue.forEach(function (u) {
	u.cooldowns = u.cooldowns.map(function (v, i) {
	    return Math.max(0, v-1);
	});

	let mapper = function (v) {
	    v.time = Math.max(0, v.time-1);
	    return (v.time > 0) ? v : null;
	};

	let filter = function (v) {
	    return v !== null;
	};

	u.atkmult = u.atkmult.map(mapper).filter(filter);
	u.defmult = u.defmult.map(mapper).filter(filter);
	u.spdmult = u.spdmult.map(mapper).filter(filter);

	u.states = u.states.map(mapper).filter(filter);
    });

    let unit = YukkuriArena.Battle.nextUnit();
    while (0 === unit.hp) {
	unit = YukkuriArena.Battle.nextUnit();
    }

    let ready = unit.skills.filter(function (v, i) {
	return (unit.cooldowns[i] === 0);
    }).sort(function (a, b) {
	return YukkuriArena.Common.random(0, 3) - 1;
    });

    let hlactor = YukkuriArena.Common.teamColor(unit);

    let effects = [];

    let skill = ready.shift();
    if (skill) {
	let fullskill = YukkuriArena.Skills.DataBase[skill];
	let name = YukkuriArena.Common.boldface(fullskill.name);
	let index = unit.skills.indexOf(skill);
	unit.cooldowns[index] = unit.basecd[index];
	YukkuriArena.Battle.report(`${hlactor} uses ${name}!`);

	let actions = [];
	if ('function' === typeof fullskill.effect) {
	    actions = [{
		area: fullskill.area,
		team: fullskill.team,
		effect: fullskill.effect
	    }];
	} else {
	    actions = fullskill.effect;
	}

	for (let action of actions) {
	    let area = action.area;
	    let team = action.team;

	    if ('self' !== team) {
		team = (('enemy' === unit.team && 'enemy' === action.team) ||
			('player' === unit.team && 'player' === action.team)) ?
		    'player' :
		    'enemy';
		team = YukkuriArena.Battle.selectTeam(team);
		team = YukkuriArena.Common.scramble(team);
	    }

	    if (1 === area) {
		effects.push({
		    effect: action.effect,
		    type: 'skill',
		    target: YukkuriArena.Battle.selectTarget(unit, team),
		});
	    } else {
		area = (-1 === area) ?
		    team.length :
		    Math.min(area, team.length);

		for (let i=0; i<area; ++i) {
		    effects.push({
			effect: action.effect,
			type: 'skill',
			target: YukkuriArena.Battle.pickTarget(unit, i, team),
		    });
		}
	    }
	}
    } else {
	let team = YukkuriArena.Battle.selectTeam(('player' === unit.team) ?
						  'enemy' :
						  'player');
	effects.push({
	    effect: YukkuriArena.Skills.attack,
	    type: 'attack',
	    target: YukkuriArena.Battle.selectTarget(unit, team),
	});
    }

    let actionable = 0;

    for (let i in effects) {
	let target = effects[i].target;
	let results = YukkuriArena.Battle.perform(unit, effects[i]);

	if (0 !== results.length) {
	    actionable = actionable + 1;
	}

	for (let i in results) {
	    YukkuriArena.Battle.report(results[i].report);
	    YukkuriArena.Battle.renderUnitStats(unit);
	    YukkuriArena.Battle.renderUnitStats(target.target);
	    for (let j in results[i].actions) {
		results[i].actions[j]();
	    }
	}
    }

    let poison = unit.states.find(function(v){return v.name==='poison'});

    if (0 !== actionable && undefined !== poison) {
	let damage = Math.floor(poison.power * (1/16) * unit.maxhp);
	let bdam = YukkuriArena.Common.boldface(damage);

	YukkuriArena.Battle.report(`${hlactor} lost ${bdam} due to poison!`);

	unit.hp = Math.max(0, unit.hp - damage);
    }

    for (let i=1; i<YukkuriArena.Battle.playerTeam.length; ++i) {
	let unit = YukkuriArena.Battle.playerTeam[i];
	YukkuriArena.Battle.renderUnitStats(unit);
    }
    for (let i=1; i<YukkuriArena.Battle.enemyTeam.length; ++i) {
	let unit = YukkuriArena.Battle.enemyTeam[i];
	YukkuriArena.Battle.renderUnitStats(unit);
    }

    if (unit.hp > 0) {
	unit.times = unit.times + 1;
	YukkuriArena.Battle.restoreUnit(unit);
    } else if (unit.hp <= 0 && undefined !== poison) {
	YukkuriArena.Battle.report(`${hlactor} has been defeated!`);
	((unit.team === 'player') ?
	 YukkuriArena.Battle.playerTeam :
	 YukkuriArena.Battle.enemyTeam).splice(unit, 1);
    }

    YukkuriArena.Battle.renderUnitOrder();

    // 1 is subtracted because it's the team name
    let utl = YukkuriArena.Battle.playerTeam.length - 1;
    let etl = YukkuriArena.Battle.enemyTeam.length - 1;

    if ((0 === utl && 0 === etl) || (0 < utl && 0 === etl)) {
	let bold = YukkuriArena.Common.boldface(YukkuriArena.Battle.playerTeam[0]);
	YukkuriArena.Battle.report(`${bold} is the winner!`);
    } else if (0 === utl && 0 < etl) {
	let bold = YukkuriArena.Common.boldface(YukkuriArena.Battle.enemyTeam[0]);
	YukkuriArena.Battle.report(`${bold} is the winner!`);
    } else if (false === YukkuriArena.Battle.paused) {
	let timeout = YukkuriArena.Battle.nextTick();
	window.setTimeout(YukkuriArena.Battle.tick, timeout);
    } else if (true === YukkuriArena.Battle.paused) {
	let button = document.getElementById('pause-resume-button');
	button.innerText = 'Resume';
    }
};

YukkuriArena.Battle.report = function (msg) {
    let p = YukkuriArena.Common.element('p');
    p.innerHTML = msg;
    let log = document.getElementById('battle-log');
    // p.scrollIntoView();
    log.append(p);
    log.scrollTop = log.scrollHeight - log.clientHeight;
    return p;
};

YukkuriArena.Battle.initPauseResume = function () {
    let button = document.getElementById('pause-resume-button');
    button.onclick = function () {
	let paused = YukkuriArena.Battle.paused;
	YukkuriArena.Battle.paused = !paused;
	if (true === paused) {
	    button.innerText = 'Pause';
	    window.setTimeout(YukkuriArena.Battle.tick, 1335);
	} else {
	    button.innerText = 'Pausing…';
	}
    };
    button.innerText = 'Pause';
};
