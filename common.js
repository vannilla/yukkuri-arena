// Yukkuri Arena - a web technologies-based game about pastries.
// Copyright (C) 2021 Alessio Vanni

// This file is part of Yukkuri Arena.

// Yukkuri Arena is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.

// Yukkuri Arena is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with Yukkuri Arena.  If not, see <https://www.gnu.org/licenses/>.

"use strict";

const YukkuriArena = {};

YukkuriArena.Common = {};

YukkuriArena.Common.element = function (type) {
    return document.createElement(type).cloneNode(true);
};

YukkuriArena.Common.boldface = function (msg) {
    return '<strong>' + msg + '</strong>';
};

YukkuriArena.Common.highlight = function (msg, color) {
    let style = 'border-bottom: 6px solid ' + color;
    return '<span style="' + style + '">' + msg + '</span>';
};

YukkuriArena.Common.teamColor = function (unit) {
    let name = `${unit.name} [#${unit.position}]`;
    return YukkuriArena.Common.highlight(name,
					 (unit.team === 'player') ?
					 'orange' :
					 'purple');
};

YukkuriArena.Common.random = function (min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
};

YukkuriArena.Common.scramble = function (array, start, end) {
    let copy = array.slice(start, end);

    for (let i=copy.length-1; i>0; --i) {
	let n = YukkuriArena.Common.random(0, i);
	let temp = copy[i];
	copy[i] = copy[n];
	copy[n] = temp;
    }

    return copy;
};

YukkuriArena.Common.fromTemplate = function (string, ...subst) {
    let container = document.createElement('div');
    let collect = string;
    for (let i in subst) {
	collect = collect.replaceAll(subst[i].pattern, subst[i].value);
    }
    container.innerHTML = collect;
    return container.children[0];
};
